// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App'
import 'bulma/css/bulma.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faToggleOff, faToggleOn, faClipboard, faLanguage, faCheck } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faToggleOff);
library.add(faToggleOn);
library.add(faClipboard);
library.add(faLanguage);
library.add(faCheck);

Vue.component('font-icon', FontAwesomeIcon);

Vue.config.productionTip = true;
Vue.use(VueResource);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App :isApp="false"/>'
})
