# Translit
Translit is a method of encoding Cyrillic letters with Latin ones. You can use it here: https://anufrij.de/translit

For elementary OS users I created a native application written in Vala: https://github.com/artemanufrij/translit

### Donate
<a href="https://www.paypal.me/ArtemAnufrij">PayPal</a> | <a href="https://liberapay.com/Artem/donate">LiberaPay</a> | <a href="https://www.patreon.com/ArtemAnufrij">Patreon</a>

![screenshot](screenshots/screenshot.png)

## Features
* Spell Ckeck
* Languages:
    * Russian
    * Ukrainian
    * Belarusian

## Install from Github

### Requirements
``` bash
# Debian based
sudo apt install git nodejs

# Arch based
sudo pacman -S git nodejs
```

### Build
``` bash
# clone this repository
git clone https://gitlab.com/artemanufrij/anufrij-translit.git

# switch into project folder
cd anufrij-translit

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# (optional) create an electron app for linux
npm run package-linux
```